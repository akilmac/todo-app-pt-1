import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from 'uuid'

// received help from Kano zoom group

class App extends Component {
  state = {
    todos: todosList,
    value:""
  };
  //handle@todo
  handleAddTodo = () => {
    const newTodo = {
      userId: 1,
      id: uuidv4(),
      title: this.state.value,
      completed: false
    }
    const newTodos = [...this.state.todos, newTodo]
    console.log(newTodos)
    this.setState({todos: newTodos})
    this.setState({value:""})

  }

  handleText = (event) => {
    this.setState({value: event.target.value})
  }

  handleSubmit = (event) => {
    event.preventDefault()
    // if (event.key === "Enter") {
       this.handleAddTodo()

    }

  // }
 

  //handle delete
  handleDelete = (todoId) => {
    const newTodos = this.state.todos.filter(
      todoItem => todoItem.id !== todoId
    )
    this.setState({ todos: newTodos })
  }

  
  handleCheck = (id) => {
    let newTodos =  this.state.todos.map(todo =>{
      if(todo.id=== id){
        return{
          ...todo, completed: !todo.completed
        }
      }
      return {
        ...todo
      }
    }) 
    this.setState({
      todos: newTodos
    })

    
  }

  handleDelete = todoId => {
    const newTodos = this.state.todos.filter(
      todoItem => todoItem.id !== todoId
    )
    this.setState({
      todos: newTodos
    })
  }

  handleDeleteComplete= () => {
    const newTodos = this.state.todos.filter(
      todoItem => todoItem.completed !== true
    )
    this.setState({ todos: newTodos })

  }
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <form onSubmit={this.handleSubmit}>
          <input type="text" className="new-todo" placeholder="What needs to be done?" autoFocus value={this.state.value} onChange={this.handleText} />
          </form>
          
        </header>
        <TodoList 
        todos={this.state.todos}
        handleCheck= {this.handleCheck}
        handleDelete= {this.handleDelete}
        handleDeleteComplete= {this.handleDeleteComplete}        
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button 
          className="clear-completed"
          onClick={this.handleDeleteComplete}
          >Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
          onChange= {(event) => this.props.handleCheck(this.props.id)} 
          className="toggle" 
          type="checkbox" 
          checked={this.props.completed}
           />
          <label>{this.props.title}</label>
          <button 
           onClick= {(event) => this.props.handleDelete(this.props.id)}
          className="destroy" />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
            id = {todo.id}
            key = {todo.id}
            handleCheck = {this.props.handleCheck}
            handleDelete = {this.props.handleDelete}
            title={todo.title} 
            completed={todo.completed} />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
